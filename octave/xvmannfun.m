% NOTE: see https://gitlab.com/mannlab-public/toposculpting/-/tree/main/README.md about "sendcom"

% https://docs.openbrush.app/user-guide/open-brush-api/api-commands
system("sendcom new");
%system("sendcom brush.type=Light"); % https://docs.openbrush.app/user-guide/brushes/brush-list
system("sendcom brush.type=Diamond");
%system("sendcom color.rgb=1.0,0.0,0.5");
system("sendcom color.rgb=1.0,0.0,0.0");
system("sendcom brush.size.set=0.2");

f=3/4;
t=5*(0:255)/256;
%c=cos(2*pi*f*t)+1; % DC offset to not have negative y values
DCoffset=1;
scale=2;
%c=sin(2*pi*f*t)+DCoffset; % DC offset to not have negative y values
c=sin(2*pi*f*t)+1/3*sin(2*pi*3*f*t)+1/5*sin(2*pi*5*f*t); % DC offset to not have negative y values
c=c*scale + DCoffset;
s=cos(2*pi*f*t)+1/3*cos(2*pi*3*f*t)+1/5*cos(2*pi*5*f*t); % DC offset to not have negative y values
s=s*scale + DCoffset;
for n = 1:255
  system(sprintf("sendcom draw.path=[%d,%d,%d],[%d,%d,%d]",s(n),c(n),t(n),s(n+1),c(n+1),t(n+1)));
end%for

