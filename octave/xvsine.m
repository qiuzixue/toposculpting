% NOTE: see https://gitlab.com/mannlab-public/toposculpting/-/tree/main/README.md about "sendcom"

% https://docs.openbrush.app/user-guide/open-brush-api/api-commands
system("sendcom new");
system("sendcom brush.move.to=0,0,0");
system("sendcom brush.size.set=0.1")
system("sendcom brush.type=Light"); % https://docs.openbrush.app/user-guide/brushes/brush-list
system("sendcom color.rgb=1.0,0.0,0.5");

ITERATIONS=128
f=3/4;
t=5*(0:(ITERATIONS-1))/ITERATIONS;
%c=cos(2*pi*f*t)+1; % DC offset to not have negative y values
c=sin(2*pi*f*t);
c=sin(2*pi*f*t)+1/3*sin(2*pi*3*f*t)+1/5*sin(2*pi*5*f*t);
%plot(t,c);

%%%%%%%%%%%%%%%%%%%%
% USED TO MOVE THE ENTIRE PLOT AROUND
%%%%%%%%%%%%%%%%%%%%
X_OFFSET=0.5;
Y_OFFSET=1.5;

draw_path = "draw.path=";
for n = 1:(ITERATIONS)
  dp = sprintf("[%d,%d,0],",t(n)+X_OFFSET,c(n)+Y_OFFSET);
  tmp = strcat(draw_path, dp);
  draw_path = tmp;
end

% printf(draw_path)
system(sprintf("sendcom %s", draw_path))

% FOR PUTTING THE "DRAWING" INTO CENTER IN MONOSCOPIC MODE
system("sendcom user.move.to=-2,7,5")
% system("sendcom user.move.to=-2,13,5")
